#pragma once
#include "prooflogging/xor_2_clauses.hpp"
#include "drat_proof.hpp"
#include "xorengine/private/assert.hpp"

#include "xorengine/Xor.hpp"
#include "xorengine/private/myXor.hpp"

#include <algorithm>
#include <functional>
#include <sstream>

namespace proof {
namespace drat {
namespace xr {
const int nVarsInXor = 4;

template<typename Types>
class ShortXor {
public:
    bool rhs = false;
    std::vector<typename Types::Var> vars;

    ShortXor(){}

    ShortXor(const xorp::Xor<Types>& other){
        vars.reserve(other.lits.size());
        rhs = other.rhs;
        for (auto lit : other.lits) {
            assert(!xorp::literal::isNegated(lit));
            vars.push_back(xorp::literal::toVar<typename Types::Var>(lit));
        }
    }

    ShortXor(bool _rhs, std::initializer_list<typename Types::Var> _vars)
        : rhs(_rhs)
        , vars(_vars)
    {
    }

    bool operator==(const ShortXor& other) const {
        bool equal = true;
        equal &= this->rhs == other.rhs;
        equal &= this->vars.size() == other.vars.size();

        auto itA = this->vars.begin();
        auto itB = other.vars.begin();

        while (equal && itA != this->vars.end()) {
            equal &= xorp::variable::equals(*itA, *itB);
            ++itA;
            ++itB;
        }

        return equal;
    }
};

template<typename Types>
std::ostream& operator<<(std::ostream& out, const ShortXor<Types>& id) {
    out << id.rhs << " = ";
    for (auto var: id.vars) {
        out << var << " ";
    }
    return out;
}

template<typename Types>
void addXor2X(const ShortXor<Types>& xr,
    AddXorCallback<typename Types::Lit> add) {

    size_t nclauses;
    if (xr.vars.size() == 0) {
        nclauses = 1;
    } else {
        nclauses = 1 << (xr.vars.size() - 1);
    }

    for (size_t i = 0; i < nclauses; i++) {
        uint32_t assignment = (i << 1);
        assignment += popCountMod2(assignment) ^ !xr.rhs;
        assert(popCountMod2(assignment) != xr.rhs);
        auto lits = number2clause<Types>(xr.vars, assignment);
        add(lits.begin(), lits.end());
    }
}

template<typename Types>
void addXor2proof(Proof<Types>& proof, const ShortXor<Types>& xr) {
    auto f = std::bind(
        &Proof<Types>::template add<AddXorCallbackIt<typename Types::Lit>>,
        &proof, std::placeholders::_1, std::placeholders::_2);
    addXor2X(xr, f);
}

template<typename Types>
void deleteXorFromProof(Proof<Types>& proof, const ShortXor<Types>& xr) {
    auto f = std::bind(
        &Proof<Types>::template del<AddXorCallbackIt<typename Types::Lit>>,
        &proof, std::placeholders::_1, std::placeholders::_2);
    addXor2X(xr, f);
}

template<typename Var>
bool cmpVar(Var a, Var b) {
    return a < b;
}

template<typename Lit>
bool cmpLit(Lit a, Lit b) {
    return cmpVar(
        xorp::literal::toVar(a),
        xorp::literal::toVar(b));
}

template<typename Types>
class AddShortHelper {
private:
        using ShortXr = ShortXor<Types>;
        using Lit = typename Types::Lit;
        using Var = typename Types::Var;
        using ClauseRef = typename Types::Clause&;
        using LitVec = typename std::vector<typename Types::Lit>;

public:
    ShortXr result;
    AddShortHelper(Proof<Types>& _proof, const ShortXr& _a, const ShortXr& _b)
        : proof(_proof)
        , a(_a)
        , b(_b)
    {
        initResultAndEliminated();
        produceProof();
    }

private:
    std::vector<Var> eliminated;

    std::vector<Lit> trail;

    std::vector<LitVec> toErase;


    Proof<Types>& proof;

    const ShortXr& a;
    const ShortXr& b;


    void decide(Var var) {
        bool isNegated = true;
        trail.push_back(xorp::variable::toLit<Lit>(var, isNegated));
    }

    // clauses will contain exactly as many literals as their depth in
    // the decision tree.
    size_t getDepth(const LitVec& c) {
        return c.size();
    }

    void addNextProofStep(bool isResultClause) {
        proof.add(trail.begin(), trail.end());

        // LOG(debug) << "add: ";
        // for (auto lit: trail) {
        //     LOG(debug) << lit << " ";
        // }
        // LOG(debug) << EOM;

        size_t currentDepth = trail.size();
        while (toErase.size() > 0) {
            LitVec& clause = toErase.back();
            if (getDepth(clause) > currentDepth) {
                proof.del(clause.begin(), clause.end());
                toErase.pop_back();
            } else {
                assert(getDepth(clause) < currentDepth);
                break;
            }
        }

        if (!isResultClause) {
            toErase.emplace_back(trail);
        }
    }

    void produceProof() {
        /*
         * The proof works by building up a decision tree (which is
         * equivalent to a resolution tree) decisions are first made
         * on variables that remain in the resulting xor. Branches
         * that satisfy th resulting xor are discarded. Other branches
         * are extended by bruteforcing the elimated variables, i.e.,
         * variables that occured in the input xors but not in the
         * result. Because RUP is able to perform any trivial
         * resolution we do not need to output a RUP clause for every
         * branching in the tree.
         */
        size_t nVars = result.vars.size() + eliminated.size();
        size_t baseLevel = result.vars.size();

        bool resultVal = false;
        const bool isResultClause = true;

        bool first = true;
        while (first || trail.size() > 0) {
            first = false;
            while (trail.size() < baseLevel) {
                resultVal ^= 1;
                decide(result.vars[trail.size()]);
            }

            if (resultVal != result.rhs) {
                // assignment is blocked by resulting XOR, keep
                // extending assignment to remaining variables.
                while (trail.size() < nVars - 1) {
                    decide(eliminated[trail.size() - result.vars.size()]);
                }
            } else {
                // assignment satisfies resulting XOR, doing nothing
                // will cause backtracking
            }

            while (true) {
                if (trail.size() == baseLevel && resultVal != result.rhs) {
                    addNextProofStep(isResultClause);
                }

                if (trail.size() == 0) {
                    break;
                }

                if (xorp::literal::isNegated(trail.back())) {
                    // backtracking completed, flip variable
                    if (trail.size() > baseLevel) {
                        addNextProofStep(!isResultClause);
                    } else {
                        resultVal ^= true;
                    }

                    trail.back() = xorp::literal::negated(trail.back());
                    break;
                } else {
                    // continue backtracking, no proof logging
                    // neccessary because RUP is able to handle
                    // trivial resolution proofs
                    trail.pop_back();
                }
            }
        }

        assert(toErase.size() == 0);
    }


    void initResultAndEliminated() {
        auto itA = a.vars.begin();
        auto itB = b.vars.begin();

        result.rhs = a.rhs ^ b.rhs;
        auto& vec = result.vars;

        while (true) {
            if (itA == a.vars.end()) {
                std::copy(itB,b.vars.end(),back_inserter(vec));
                break;
            } else if (itB == b.vars.end()) {
                std::copy(itA,a.vars.end(),back_inserter(vec));
                break;
            } else if (xorp::variable::lessThan(*itA, *itB)) {
                vec.push_back(*itA);
                itA += 1;
            } else if (xorp::variable::lessThan(*itB, *itA)) {
                vec.push_back(*itB);
                itB += 1;
            } else {
                assert(xorp::variable::equals(*itA, *itB));
                eliminated.push_back(*itA);
                itA += 1;
                itB += 1;
            }
        }
    }
};


// add two short (sorted) xors, i.e. derive all clauses of the result
template<typename Types>
ShortXor<Types> addShort(Proof<Types>& proof, const ShortXor<Types>& a, const ShortXor<Types>& b) {
    // std::stringstream s;
    // s << "add xors [" << a << "] + [" << b << "]";
    // proof.comment(s.str());
    // LOG(debug) << s.str() << EOM;

    AddShortHelper<Types> helper(proof,a,b);
    return helper.result;
}

template<typename Types>
class LongXor {
private:
    using Var = typename Types::Var;

public:
    class Iterator {
    public:
        const LongXor& longXr;
        typename std::vector<ShortXor<Types>>::const_iterator xrIt;
        typename std::vector<Var>::const_iterator varIt;
        typename std::vector<Var>::const_iterator connectionIt;

        Iterator(const LongXor& _longXr, bool end = false)
            : longXr(_longXr)
            , xrIt(longXr.xrs.begin())
            , varIt(xrIt->vars.begin())
            , connectionIt(longXr.connectionVars.begin())
        {
            if (end) {
                xrIt = longXr.xrs.end();
            }
            initVarIt();
        }

        void nextXr() {
            if (xrIt != longXr.xrs.end()) {
                ++xrIt;
                if (xrIt != longXr.xrs.end()) {
                    varIt = xrIt->vars.begin();
                    initVarIt();
                }
            }
        }

        bool operator==(const Iterator& other) const {
            if (&this->longXr != &other.longXr
                    || this->xrIt != other.xrIt)
            {
                return false;
            } else if (this->xrIt != this->longXr.xrs.end()
                && this->varIt != other.varIt)
            {
                return false;
            } else {
                return true;
            }
        }

        bool operator!=(const Iterator& other) const {
            return !((*this) == other);
        }

        void initVarIt() {
            if (xrIt != longXr.xrs.end()) {
                if (varIt == xrIt->vars.end()) {
                    ++xrIt;

                    assert(xrIt == longXr.xrs.end());
                    assert(longXr.xrs.size() == 1);
                } else if (connectionIt != longXr.connectionVars.end()
                    && xorp::variable::equals(*varIt, *connectionIt)) {
                        ++(*this);
                }
            }
        }

        bool skipConnectionVar() {
            return (connectionIt != longXr.connectionVars.end()
                    && xorp::variable::equals(*connectionIt,*varIt));
        }

        Var operator*() {
            return *varIt;
        }

        Iterator operator++(int) {
            Iterator result = *this;
            ++(*this);
            return result;
        }

        Iterator operator++() {
            while (true) {
                ++varIt;
                if (varIt == xrIt->vars.end()) {
                    ++xrIt;
                    if (xrIt == longXr.xrs.end()) {
                        break;
                    } else {
                        varIt = xrIt->vars.begin();
                        assert(varIt != xrIt->vars.end());
                    }
                }
                if (skipConnectionVar()) {
                    ++connectionIt;
                } else {
                    break;
                }
            }

            return *this;
        }
    };

    std::vector<ShortXor<Types>> xrs;
    std::vector<Var> connectionVars;

    LongXor(const LongXor& other)
        : xrs(other.xrs)
        , connectionVars(other.connectionVars)
    {

    }

    // LongXor(const LongXor& other) = delete;

    LongXor(LongXor&& other)
        : xrs(std::move(other.xrs))
        , connectionVars(std::move(other.connectionVars))
    {

    }

    LongXor& operator=(LongXor&& other) {
        xrs = std::move(other.xrs);
        connectionVars = std::move(other.connectionVars);
        return *this;
    }

    LongXor(ShortXor<Types>&& xr) {
        xrs.emplace_back(std::move(xr));
        sort();
    }

    LongXor(std::vector<ShortXor<Types>>&& _xrs)
        : xrs(std::move(_xrs))
    {
        if (xrs.size() > 1) {
            detectConnectionVars();
        }
        sort();
        assert(xrs.size() > 0);
    }

    void detectConnectionVars() {
        auto it = xrs.begin();
        assert(it != xrs.end());
        assert(it->vars.size() >= 1);
        connectionVars.push_back(it->vars.back());
        ++it;
        while (it != xrs.end()) {
            assert(it->vars.size() > 1);
            auto& a = it->vars.front();
            auto& b = it->vars.back();

            ++it;
            if (it == xrs.end()) {
                connectionVars.push_back(a);
            } else {
                if (b < a) {
                    std::swap(a,b);
                }

                connectionVars.push_back(a);
                connectionVars.push_back(b);
            }
        }
    }

    void sort() {
        for (auto& xr: xrs) {
            std::sort(xr.vars.begin(), xr.vars.end(), [ ]( const auto& lhs, const auto& rhs )
            {
               return xorp::variable::lessThan(lhs, rhs);
            });
        }
    }

    bool rhs() {
        bool result = false;
        for (const ShortXor<Types>& xr: xrs) {
            result ^= xr.rhs;
        }
        return result;
    }

    Iterator begin() const {
        return Iterator(*this);
    }

    Iterator end() const {
        return Iterator(*this, true);
    }

    bool operator==(const LongXor& other) const {
        Iterator me = this->begin();
        Iterator them = other.begin();

        while (me != this->end() && them != other.end()) {
            if (*me != *them) {
                return false;
            }
            ++me;
            ++them;
        }

        if (me != this->end() || them != other.end()) {
            return false;
        }

        return true;
    }

    LongXor() {}
};

template<typename Types>
std::ostream& operator<<(std::ostream& out, const LongXor<Types>& xr) {
    const char* reset = "\033[0m";
    const char* blue  = "\033[34m";

    auto connectionIt = xr.connectionVars.begin();


    for (const ShortXor<Types>& sxr : xr.xrs) {
        out << sxr.rhs << " = ";
        for (auto var: sxr.vars) {
            bool color = false;

            if (connectionIt != xr.connectionVars.end()) {
                if (xorp::variable::equals(var, *connectionIt)) {
                    ++connectionIt;
                    color = true;
                }
            }

            if (color) {out << blue;}

            out << var << " ";

            if (color) {out << reset;}
        }

        out << "; ";
    }

    assert(connectionIt == xr.connectionVars.end());

    return out;
}

template<typename Types>
class LongXorDeriver {
private:
    using Var = typename Types::Var;

public:
    Proof<Types>& proof;
    ShortXor<Types> lastXr;
    LongXor<Types> longXr;
    const size_t maxXorSize = nVarsInXor;

    LongXorDeriver(Proof<Types>& _proof)
        : proof(_proof)
    {
        longXr.xrs.emplace_back();
    }

    void push_back(Var var) {
        if (longXr.xrs.back().vars.size() + 2 > maxXorSize) {
            Var newVar = proof.newVar();
            longXr.xrs.back().vars.push_back(newVar);
            longXr.xrs.emplace_back();
            longXr.xrs.back().vars.push_back(newVar);
        }
        longXr.xrs.back().vars.push_back(var);
    }

    void setRhs(bool rhs) {
        longXr.xrs.front().rhs = rhs;
    }

    void prepareIntermediate(ShortXor<Types>& xr) {
        Var a = xr.vars.front();
        Var b = xr.vars.back();
        std::sort(xr.vars.begin(), xr.vars.end(), [ ]( const auto& lhs, const auto& rhs )
        {
            return xorp::variable::lessThan(lhs, rhs);
        });
        for (Var var: xr.vars) {
            if (xorp::variable::equals(var, a)) {
                longXr.connectionVars.push_back(a);
                longXr.connectionVars.push_back(b);
                break;
            } else if (xorp::variable::equals(var, b)) {
                longXr.connectionVars.push_back(b);
                longXr.connectionVars.push_back(a);
                break;
            }
        }
    }


    /* assumes that the connection variables are in first and last position
     * of every XOR, except the first and last XOR. The first (last)
     * XOR has no connection variable in the first (last) position.
     */
    void prepare() {
        lastXr = std::move(longXr.xrs.back());
        longXr.xrs.pop_back();

        bool first = true;
        for (ShortXor<Types>& xr: longXr.xrs) {
            // last variable will be used as definition
            std::swap(xr.vars.front(), xr.vars.back());
            // constraints will be RAT on first variable

            // std::stringstream s;
            // s << "add new xor with fresh variable: " << xr;
            // proof.comment(s.str());

            addXor2proof(proof, xr);
            if (first) {
                first = false;
                longXr.connectionVars.push_back(xr.vars.front());
                std::sort(xr.vars.begin(), xr.vars.end(), [ ]( const auto& lhs, const auto& rhs )
                {
                   return xorp::variable::lessThan(lhs, rhs);
                });
            } else {
                prepareIntermediate(xr);
            }

        }
    }

    void finalize() {
        if (longXr.xrs.size() != 0) {
            assert(lastXr.vars.size() > 0);
            longXr.connectionVars.push_back(lastXr.vars.front());
        }
        std::sort(lastXr.vars.begin(), lastXr.vars.end(), [ ]( const auto& lhs, const auto& rhs )
        {
            return xorp::variable::lessThan(lhs, rhs);
        });
        longXr.xrs.emplace_back(std::move(lastXr));
    }
};


template<typename Types>
typename LongXor<Types>::Iterator& min(typename LongXor<Types>::Iterator& a, typename LongXor<Types>::Iterator& b){
    typename LongXor<Types>::Iterator aEnd(a.longXr, true);
    typename LongXor<Types>::Iterator bEnd(b.longXr, true);

    if (b == bEnd) {
        return a;
    } else if (a == aEnd) {
        return b;
    } else if (xorp::variable::lessThan(*a, *b)) { //todo: note this was <= remove comment if it works
        return a;
    } else {
        return b;
    }
}

template<typename Types>
LongXor<Types> addLong(Proof<Types>& proof, LongXor<Types>& a, LongXor<Types>& b) {
    using LongXrIterator = typename LongXor<Types>::Iterator;

    LongXorDeriver<Types> result(proof);

    LongXrIterator itA = a.begin();
    LongXrIterator itB = b.begin();

    while (true) {
        if (itA == a.end()) {
            for (;itB != b.end(); ++itB) {
                result.push_back(*itB);
            }
            break;
        } else if (itB == b.end()) {
            for (;itA != a.end(); ++itA) {
                result.push_back(*itA);
            }
            break;
        } else if (xorp::variable::lessThan(*itA, *itB)) {
            result.push_back(*itA);
            ++itA;
        } else if (xorp::variable::lessThan(*itB, *itA)) {
            result.push_back(*itB);
            ++itB;
        } else {
            ++itA;
            ++itB;
        }
    }

    result.setRhs(a.rhs() ^ b.rhs());

    result.prepare();


    {
        LongXrIterator itA = a.begin();
        LongXrIterator itB = b.begin();
        LongXor<Types>& c = result.longXr;
        LongXrIterator itC = c.begin();

        assert(itA != a.end());
        assert(itB != b.end());

        LongXrIterator& it = min<Types>(itA, itB);
        ShortXor<Types> remainder(*it.xrIt);
        it.nextXr();

        bool first = true;

        while (itA != a.end() || itB != b.end() || itC != c.end()) {
            LongXrIterator&  it = min<Types>(itA, min<Types>(itB, itC));

            if (itA != a.end()) LOG(debug) << *itA << " ";
            if (itB != b.end()) LOG(debug) << *itB << " ";
            if (itC != c.end()) LOG(debug) << *itC << " ";
            LOG(debug) << "choosen: " <<  *it << EOM;

            ShortXor<Types> newRemainder = addShort(proof, *it.xrIt, remainder);
            if (!first) {
                deleteXorFromProof(proof, remainder);
            } else {
                first = false;
            }
            std::swap(newRemainder, remainder);
            it.nextXr();
        }

        result.finalize();

        LOG(debug) << "result: ";
        for (auto xr: result.longXr.xrs) {
            LOG(debug) << xr << " + ";
        }
        LOG(debug) << EOM;

        LOG(debug) << remainder << " " << result.longXr.xrs.back() << EOM;
        assert(remainder == result.longXr.xrs.back());
    }
    return result.longXr;
}

}}} // closing namespaces