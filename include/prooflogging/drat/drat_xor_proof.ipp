#include "drat_xor_proof.hpp"
#include "drat_proof.hpp"
#include "drat_xor_prooflogging.hpp"

#include "prooflogging/ConstraintId.hpp"

namespace proof {
namespace drat {
namespace xr {

template<typename Types>
XorHandle<Types>::~XorHandle() = default;

template<typename Types>
XorHandle<Types>::XorHandle() = default;

template<typename Types>
XorHandle<Types>::XorHandle(XorHandle<Types>&& other) = default;

template<typename Types>
XorHandle<Types>::XorHandle(const XorHandle<Types>& other) {
    if (other.xr) {
        xr = std::make_unique<LongXor<Types>>(*other.xr);
    }
}

template<typename Types>
XorHandle<Types>& XorHandle<Types>::operator=(const XorHandle<Types>& other) {
    if (other.xr) {
        xr = std::make_unique<LongXor<Types>>(*other.xr);
    }
    return *this;
}


template<typename Types>
XorHandle<Types>::XorHandle(LongXor<Types>&& _xr)
    : xr(std::make_unique<LongXor<Types>>(std::move(_xr)))
{}

template<typename Types>
XorHandle<Types>& XorHandle<Types>::operator=(XorHandle<Types>&& other) = default;

template<typename Types>
XorHandle<Types> newXorHandleFromProofTree(drat::Proof<Types>&, xorp::Xor<Types>& _xr, xorp::BDD&) {
    LongXor<Types> result(_xr);
    return XorHandle<Types>(std::move(result));
}

template<typename Types>
XorHandle<Types> xorSum(drat::Proof<Types>& proof, const std::vector<XorHandle<Types>>& v) {
    assert(v.size() > 0);
    auto it = v.begin();

    LongXor<Types> sum(*it->xr);
    ++it;

    bool first = true;
    for (; it != v.end(); ++it) {
        LOG(debug) << "xorSum()::step [" << *it->xr << "] + [" << sum << "]" << EOM;
        LongXor<Types> newSum = addLong(proof, sum, *it->xr);
        if (!first) {
            for (auto& xr: sum.xrs) {
                deleteXorFromProof(proof, xr);
            }
        } else {
            first = false;
        }
        std::swap(sum, newSum);
        LOG(debug) << "xorSum()::result [" << sum << "]" << EOM;
        LOG(debug) << "xorSum()::conection = " << it->xr->connectionVars.size() << EOM;
    }

    return XorHandle<Types>(std::move(sum));
}

template<typename Types>
ConstraintId reasonGeneration(drat::Proof<Types>& proof, const XorHandle<Types>&, const std::vector<typename Types::Lit>& clause) {
    // Technically, we don't need to do anything because the reason
    // clause is already produced when adding the XORs, so DRAT will
    // find them. However, we want to be able to remove the clauses so
    // lets just add them anyway.

    proof.add(clause.begin(), clause.end());
    return ConstraintId::undef();
}

template<typename Types>
void deleteXor(drat::Proof<Types>& proof, const XorHandle<Types>& handle) {
    proof.comment("erasing xor handler");
    for (auto& xr: handle.xr->xrs) {
        deleteXorFromProof(proof, xr);
    }
}

}}} // closing namespaces