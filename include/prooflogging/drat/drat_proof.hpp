#pragma once

#include <fstream>
#include <string>
#include <functional>
#include <ostream>

#include "prooflogging/ConstraintId.hpp"
#include "xorengine/SolverAdapter.hpp"
#include "prooflogging/FastStream.hpp"

#include "drat_LitPrinter.hpp"

const char* const endl = "\n";

namespace proof {
namespace drat {

template<typename Types>
class Proof {
    using Solver = xorp::Solver<Types>;
    using Var = typename Types::Var;

    size_t formulaRead = 0;
    Solver& solver;
    FastStream stream;

public:
    Proof(std::string filename, Solver& _solver, size_t = 0):
        solver(_solver),
        stream(filename)
    {
        comment("Contains proof steps by XorEngine.");
    }

    ConstraintId getNextFormulaConstraintId() {
        formulaRead += 1;
        return ConstraintId{formulaRead};
    }

    void comment(std::string comment) {
        stream << "c " << comment << endl;
    }


    template<typename T>
    void del(T begin, T end) {
        stream << "d ";
        add(begin,end);
    }

    template<typename T>
    void find(T begin, T end) {
        stream << "f ";
        add(begin,end);
    }


    template<typename T>
    void add(T begin, T end) {
        T it = begin;
        while (it != end) {
            auto lit = *it;
            ++it;

            stream << LitPrinter(solver.getLitPrintName(lit)) << " ";
        }
        stream << "0\n";
    }

    Var newVar() {
        return solver.getFreshVar();
    }
};
}} // close namespaces