#include "gtest/gtest.h"
#include "test/SolverMock.h"
#include "XorDetector.h"

#include <iostream>
#include <fstream>

TEST(BDD, twoXorOk) {
    BDD proofTree;
    proofTree.nVars = 2;
    proofTree.rhs = true;

    std::vector<testImpl::Clause> clauses = {
        { 1, 2},
        {-1,-2}
    };

    for (auto& clause: clauses) {
        proofTree.addClause(clause);
    }

    EXPECT_TRUE(proofTree.isHappy());
}

TEST(BDD, twoXorNotOk) {
    BDD proofTree;
    proofTree.nVars = 2;
    proofTree.rhs = false;

    std::vector<testImpl::Clause> clauses = {
        { 1, 2},
        {-1,-2}
    };

    for (auto& clause: clauses) {
        proofTree.addClause(clause);
    }

    std::cout << "bdd:\n";
    std::cout << proofTree;
    EXPECT_FALSE(proofTree.isHappy());
}


TEST(BDD, twoXorOk2) {
    BDD proofTree;
    proofTree.nVars = 2;
    proofTree.rhs = false;

    std::vector<testImpl::Clause> clauses = {
        { -1, 2},
        {  1,-2}
    };

    for (auto& clause: clauses) {
        proofTree.addClause(clause);
    }

    std::cout << "bdd:\n";
    std::cout << proofTree;
    EXPECT_TRUE(proofTree.isHappy());
}

TEST(BDD, threePartial) {
    BDD proofTree;
    proofTree.nVars = 3;
    proofTree.rhs = true;

    std::vector<testImpl::Clause> clauses = {
        {  1, 2, 3},
        {  1,-2},
        { -1},
    };

    for (auto& clause: clauses) {
        proofTree.addClause(clause);
    }

    std::cout << "bdd:\n";
    std::cout << proofTree;
    EXPECT_TRUE(proofTree.isHappy());
}

TEST(BDD, exactlyOne) {
    BDD proofTree;
    proofTree.nVars = 4;
    proofTree.rhs = true;

    std::vector<testImpl::Clause> clauses = {
        {  1, 2, 3, 4},
        { -1,-2},
        { -1,-3},
        { -1,-4},
        { -2,-3},
        { -2,-4},
        { -3,-4},
    };

    for (auto& clause: clauses) {
        proofTree.addClause(clause);
    }

    std::cout << "bdd:\n";
    std::cout << proofTree;
    EXPECT_TRUE(proofTree.isHappy());
}



TEST(BDD, exactlyTwoOfFour) {
    BDD proofTree;
    proofTree.nVars = 4;
    proofTree.rhs = false;

    std::vector<testImpl::Clause> clauses = {
        {-1, -2, -3},
        {-1, -2, -4},
        {-1, -3, -4},
        {-2, -3, -4},
        { 1,  2,  3},
        { 1,  2,  4},
        { 1,  3,  4},
        { 2,  3,  4}};


    size_t i = 0;
    for (auto& clause: clauses) {
        clause.id = proof::ConstraintId{++i};
        proofTree.addClause(clause);
    }

    std::ofstream stream("out.dot");
    proofTree.toDOT(stream);
    EXPECT_TRUE(proofTree.isHappy());
}