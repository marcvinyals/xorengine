import unittest
import os

from pathlib import Path

from veripb import run, InvalidProof, ParseError
from veripb.utils import Settings as MiscSettings
from veripb.verifier import Verifier

class TestIntegration(unittest.TestCase):
    def run_single(self, formulaPath):
        proofPath = formulaPath.with_suffix(".drat")
        print("veripb %s %s"%(formulaPath, proofPath))

        miscSettings = MiscSettings({"drat": True})
        verifierSettings = Verifier.Settings({"trace": True})
        with formulaPath.open() as formula:
            with proofPath.open() as proof:
                run(formula, proof, verifierSettings = verifierSettings, miscSettings = miscSettings)

    def correct_proof(self, formulaPath):
        self.run_single(formulaPath)

    def incorrect_proof(self, formulaPath):
        try:
            self.run_single(formulaPath)
        except InvalidProof as e:
            pass
        else:
            self.fail("Proof should be invalid.")

    def parsing_failure(self, formulaPath):
        try:
            self.run_single(formulaPath)
        except ParseError as e:
            pass
        else:
            self.fail("Parsing should fail.")

def create(formulaPath, helper):
    def fun(self):
        helper(self, formulaPath)
    return fun

def findProblems(globExpression):
    current = Path.cwd()
    files = current.glob(globExpression)
    files = [f for f in files if f.suffix in [".cnf",".opb"] and f.is_file()]
    return files

correct = findProblems("./generated_tests/drat/**/*.*")
for file in correct:
    method = create(file, TestIntegration.correct_proof)
    method.__name__ = "test_%s"%(file.stem)
    setattr(TestIntegration, method.__name__, method)



if __name__=="__main__":
    try:
        import pytest
    except NameError:
        unittest.main()
    else:
        pytest.main([__file__])