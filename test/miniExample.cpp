#include "test/Solver.h"
#include "myXor.h"

using namespace testImpl;
using namespace xorp;

int main(int argc, char const *argv[])
{
    typedef std::vector<Lit> lv;
    std::vector<Xor> xors;
    xors.emplace_back(false, lv{Lit(1), Lit(2), Lit(3)});
    xors.emplace_back(false, lv{Lit(4)});
    xors.emplace_back(true, lv{Lit(2), Lit(3)});

    testImpl::Solver s;
    xorp::Solver wrapper(s);

    XorPropagator prop(wrapper, xors);

    return 0;
}