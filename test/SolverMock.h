#pragma once
#include "Solver.h"
#include "gmock/gmock.h"

#include <algorithm>
#include <initializer_list>

using ::testing::_;
using ::testing::AnyNumber;
using ::testing::Return;

using namespace testImpl;

class SolverMock: public Solver {

public:
    MOCK_METHOD(bool, isSet, (Var v), (override));
    MOCK_METHOD(bool, isTrue, (Var v), (override));
    MOCK_METHOD(Lit, trail, (size_t pos), (override));
    MOCK_METHOD(size_t, trailSize, (), (override));
    MOCK_METHOD(void, conflict, (const xorp::Reason& clause), (override));
    MOCK_METHOD(void, enqueue, (Lit l, const xorp::Reason& clause), (override));

    void setTrail(std::vector<Lit> trail) {
        EXPECT_CALL(*this, isSet(_))
            .WillRepeatedly(Return(false));
        EXPECT_CALL(*this, isTrue(_))
            .Times(0);

        for (size_t i = 0; i < trail.size(); i++) {
            Lit& l = trail[i];
            EXPECT_CALL(*this, isSet(l.var()))
                .WillRepeatedly(Return(true));
            EXPECT_CALL(*this, isTrue(l.var()))
                .WillRepeatedly(Return(!l.isNegated()));
            EXPECT_CALL(*this, trail(i))
                .WillRepeatedly(Return(l));
        }

        EXPECT_CALL(*this, trailSize())
            .WillRepeatedly(Return(trail.size()));
    }
};

MATCHER_P(PropEq, value, "") {
    std::vector<Lit> expected(value);
    assert(value.size() > 0);
    std::sort(expected.begin() + 1, expected.end());
    std::vector<Lit> actual(arg.clause);
    assert(actual.size() > 0);
    std::sort(actual.begin() + 1, actual.end());
    return expected == actual;
}

MATCHER_P(ConflictEq, value, "") {
    std::vector<Lit> expected(value);
    std::sort(expected.begin(), expected.end());
    std::vector<Lit> actual(arg.clause);
    std::sort(actual.begin(), actual.end());
    return expected == actual;
}
