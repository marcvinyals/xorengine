#include "Solver.h"
#include "xorengine/private/myXor.hpp"
#include "xorengine/XorPropagator.hpp"

using namespace testImpl;

void Solver::conflict(const xorp::Reason<testImpl::Lit>& reason) {
    LOG(trace) << "conflict: ";
    for (Lit lit:reason.clause) {
        LOG(trace) << lit << " ";
    }
    LOG(trace) << EOM;
}

void Solver::enqueue(Lit l, const std::function<xorp::Reason<testImpl::Lit>(Lit l)> getReason) {
    // for testing we are mostly interested in the clause and are
    // not interested in making use of lazy clause generation so
    // lets use the clausal interface
    xorp::Reason<testImpl::Lit> reason = getReason(l);
    enqueue(l, reason);
}

void Solver::enqueue(Lit l, const xorp::Reason<testImpl::Lit>& reason) {
    LOG(trace) << "propagate " << l << " by ";
    for (Lit lit:reason.clause) {
        LOG(trace) << lit << " ";
    }
    LOG(trace) << EOM;
}