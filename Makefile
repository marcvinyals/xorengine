.PHONY: test

all:
	make xorfinder_pbp -C ./build/debug/test

test:
	set -e; make test_all -C ./build/debug